package com.intouch.MOPH.api


import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import kiwi.test.com.api.ServiceInterface
import kiwi.test.com.helpers.Constants
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object ApiManager {

    private var retrofit: Retrofit? = null

    val client: Retrofit?
        get() {
            val gson = GsonBuilder().setLenient().create()

            val logging = HttpLoggingInterceptor()
            logging.setLevel(HttpLoggingInterceptor.Level.BODY)

            val client = OkHttpClient.Builder()
                    .addInterceptor(logging)
                    .connectTimeout(10, TimeUnit.SECONDS)
                    .readTimeout(7, TimeUnit.SECONDS).build()

            retrofit = Retrofit.Builder()
                    .baseUrl(Constants.API_URL)
                    .client(client)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(Gson()))
                    .build()
            return retrofit
        }

    val service = client!!.create(ServiceInterface::class.java)
}