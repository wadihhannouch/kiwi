package kiwi.test.com.api;



import io.reactivex.Observable;
import kiwi.test.com.Models.FlightResult;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;
public interface ServiceInterface {

    @Headers({"Accept: application/json", "Content-Type: application/json"})
    @GET("/flights")
    Observable<FlightResult> getFlights (
            @Query("flyFrom") String flyFrom,
            @Query("to") String flyTo,
            @Query("dateFrom") String dateFrom,
            @Query("dateTo") String dateTo,
            @Query("selected_cabins") String selected_cabins,
            @Query("partner") String partner,
            @Query("sort") String sort,
            @Query("v") String dataVersion,
            @Query("limit") String limit


    );

//    v=2&
//    sort=popularity&
//    asc=0&
//    locale=en&
//    daysInDestinationFrom=&
//    daysInDestinationTo=&
//    affilid=&children=0&
//    infants=0&
//    flyFrom=49.2-16.61-250km
//    &to=anywhere
//    &featureName=aggregateResults&
//    dateFrom=06/03/2017&
//    dateTo=06/04/2017&
//    typeFlight=oneway&
//    returnFrom=&returnTo=&
//    one_per_date=0&
//    oneforcity=1&
//    wait_for_refresh=0&
//    adults=1&limit=45

    @Headers({"Accept: application/json", "Content-Type: application/json"})
    @GET("/locations")
    Observable<Object> getLocations ();
//    @Headers({"Accept: application/json", "Content-Type: application/json"})
//    @GET("/api/eservices_departments")
//    Observable<Department> getDepartments(@Query("lang") String lang);
//
//    @Headers({"Accept: application/json", "Content-Type: application/json"})
//    @GET("/api/eservices_categories")
//    Observable<Department> getCategories(@Query("lang") String lang);
//
//    @Headers({"Accept: application/json", "Content-Type: application/json"})
//    @GET("/api/form_details/{id}")
//    Observable<FormDetails> getFormDetail(@Path("id") int id);
//
//    @GET
//    Observable<ResponseBody> downloadFileWithDynamicUrlSync(@Url String fileUrl);
//
//    @Headers({"Accept: application/json", "Content-Type: application/json"})
//    @GET("/api/service_details/")
//    Observable<ServiceDetails> getServiceDetails(@Query("lang") String lang,
//                                                 @Query("catId") Integer catId,
//                                                 @Query("serviceId") Integer serviceId,
//                                                 @Query("catFlag") Integer catFlag);
//
//    @Headers({"Accept: application/json", "Content-Type: application/json"})
//    @GET("/api/getGlobalVariables")
//    Observable<GlobalVariables> getGlobalVariebls(@Query("lang") String lang);
//
//    @FormUrlEncoded
//    @POST("/api/register")
//    Observable<Registration> register(@Field("firstName") String firstName,
//                                      @Field("lastName") String lastName,
//                                      @Field("email") String email,
//                                      @Field("gender") String gender,
//                                      @Field("country_id") String country_id,
//                                      @Field("password") String password,
//                                      @Field("phone_number") String phone_number,
//                                      @Field("address") String address);
//
//    @FormUrlEncoded
//    @POST("/api/login")
//    Observable<Response<Registration>> login(@Field("email") String email,
//                                             @Field("password") String password);
//
//    @FormUrlEncoded
//    @POST("/api/change_password")
//    Observable<Registration> changePassword(@Header("AccessToken") String token,
//                                            @Field("old_password") String oldPassword,
//                                            @Field("new_password") String newPassword);
//
//    @FormUrlEncoded
//    @POST("/api/forgot_password")
//    Observable<Registration> forgotPassword(@Field("email") String email);
//
//    @FormUrlEncoded
//    @POST("/api/search_forms")
//    Observable<ServiceDetails> search(@Field("text") String text);
//
//    @POST("/api/track_document")
//    Observable<DocumentsResponse> getTrackDocuments(@Header("AccessToken") String token);
}
