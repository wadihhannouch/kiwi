package kiwi.test.com.Main

import android.os.Bundle
import android.view.View
import kiwi.test.com.Models.FlightResult
import kotlinx.android.synthetic.main.activity_main.*
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import kiwi.test.com.BaseActivity
import kiwi.test.com.R

class MainActivity() : BaseActivity<MainContract.View, MainContract.Presenter>(),
    MainContract.View {

    lateinit var rootView: MainContract.View
    override var mPresenter: MainContract.Presenter =
        MainActivityPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        shimmer_view_container.startShimmerAnimation()
        mPresenter.onStartView()
    }

    companion object {
        lateinit var myFlightResult : FlightResult
    }

    override fun flightResults(flightResult: FlightResult,dateFrom: String,dateTo: String) {
        shimmer_view_container.stopShimmerAnimation();
        shimmer_view_container.visibility = View.GONE
        myFlightResult = flightResult
        flightsRecylcerView.setLayoutManager(LinearLayoutManager(applicationContext, LinearLayoutManager.HORIZONTAL, false))
        val snapHelper = LinearSnapHelper()
        snapHelper.attachToRecyclerView(flightsRecylcerView)
        flightsRecylcerView.setItemAnimator(DefaultItemAnimator())
        flightsRecylcerView.setAdapter(
            FlightsListAdapter(
                myFlightResult,
                dateFrom,
                dateTo,
                context
            )
        )

    }
}