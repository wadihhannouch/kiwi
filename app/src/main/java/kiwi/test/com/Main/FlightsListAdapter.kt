package kiwi.test.com.Main

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import android.view.View;
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy

import kiwi.test.com.Models.FlightResult
import kiwi.test.com.R
import kiwi.test.com.helpers.Constants.Base_Image_URL
import kotlinx.android.synthetic.main.flight_item.view.*

class FlightsListAdapter (flightResult: FlightResult,dateFrom:String,dateTo :String,val context : Context ) : RecyclerView.Adapter<FlightsListAdapter.ViewHolder>(){
    var flightResult = flightResult
    var dateFrom = dateFrom
    var dateTo = dateTo
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.flight_item,
                parent,
                false
            )
        )
    }
    override fun getItemCount(): Int {
        if(flightResult.flight.size > 5 )
            return 5
        else
            return flightResult.flight.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val cityFrom = flightResult.flight[position].cityFrom
        val cityTo = flightResult.flight[position].cityTo
        val countryFrom = flightResult.flight[position].countryFrom.name
        val countryTo = flightResult.flight[position].countryTo.name

        val price = flightResult.flight[position].price
        val curr = flightResult.currency
        val flightPrice = "$price $curr"

        val flightDuration = flightResult.flight[position].flyDuration
        var flightDistance = flightResult.flight[position].distance
        var flightStops = flightResult.flight[position].routes.size
        val mapIdto =  flightResult.flight[position].mapIdto
        val bookingURL = flightResult.flight[position].deepLink
        val imageURL = Base_Image_URL + mapIdto + ".jpg"

        holder.txtCityFrom.text = "$cityFrom - $countryFrom"
        holder.txtCityTo.text = "$cityTo - $countryTo"
        holder.txtFlyDuration.text = flightDuration
        holder.txtPrice.text = flightPrice
        holder.txtDistance.text = "$flightDistance KM "
        holder.txtStops.text = flightStops.toString()
        holder.txtDepartureDate.text = dateFrom
        holder.txtReturnDate.text = dateTo

        Glide.with(context)
            .load(imageURL)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .centerCrop()
            .into(holder.image);

        holder.btnBookHere.setOnClickListener {
            val openURL = Intent(Intent.ACTION_VIEW)
            openURL.data = Uri.parse(bookingURL)
            context.startActivity(openURL)
        }
    }

    class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {
        val image = view.destinationImageView
        var txtCityFrom = view.txtCityFrom
        var txtCityTo = view.txtCityTo
        var txtFlyDuration = view.txtFlyDuration
        var txtPrice = view.txtPrice
        var txtDistance = view.txtDistance
        var txtStops = view.txtStops
        var txtDepartureDate = view.txtDepartureDate
        var txtReturnDate = view.txtReturnDate
        var btnBookHere = view.btnBookHere
    }
}