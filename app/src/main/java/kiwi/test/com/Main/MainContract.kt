package kiwi.test.com.Main

import kiwi.test.com.BaseContract
import kiwi.test.com.Models.FlightResult


object MainContract {
    interface View : BaseContract.ActivityView {
        fun flightResults(flightResult: FlightResult,dateFrom :String , dateTo: String)
    }

    interface Presenter :
        BaseContract.ActivityPresenter<View> {
        fun onStartView()
    }
}