package kiwi.test.com.Main

import com.intouch.MOPH.api.ApiManager
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kiwi.test.com.BaseActivityPresenter
import kiwi.test.com.Models.FlightResult
import kiwi.test.com.helpers.Constants
import kiwi.test.com.helpers.RestUtils
import java.text.SimpleDateFormat
import java.util.*
import java.text.ParseException


class MainActivityPresenter : BaseActivityPresenter<MainContract.View>(),
        MainContract.Presenter {

        override fun onStartView() {
                getRandomFlightDates()

        }
        companion object{
                lateinit var dateFrom : String
                lateinit var dateTo : String
        }

        private fun getRandomFlightDates() {
                val sdf = SimpleDateFormat("dd/M/yyyy",Locale.getDefault())
                dateFrom = sdf.format(Date())
                val cal = Calendar.getInstance()
                try {
                        cal.time = sdf.parse(dateFrom)
                } catch (e: ParseException) {
                        e.printStackTrace()
                }
                val random = (4..12).random()
                cal.add(Calendar.DATE, random)  // number of days to add, can also use Calendar.DAY_OF_MONTH in place of Calendar.DATE
                val sdf1 = SimpleDateFormat("dd/M/yyyy",Locale.getDefault())
                dateTo = sdf1.format(cal.time)
                getFlights(
                        dateFrom,
                        dateTo
                )
        }

        fun getFlights(fromDate: String,toDate: String ){
                val observable = ApiManager.service.getFlights("BEY",
                        "",
                        fromDate,
                        toDate,
                        Constants.flightType.economy.type,
                        Constants.Partner,
                        Constants.sort,
                        Constants.dataVersion,
                        Constants.limit)
                mCompositeDisposable?.add(observable
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                { response -> handleResponse(response) },
                                        { error -> handleError(RestUtils.ErrorMessages(error)) }
                        ))
        }
        fun handleResponse(flightResult: FlightResult) {
                mView?.flightResults(flightResult,
                        dateFrom,
                        dateTo
                )
        }

        fun handleError(str: String?) {
                mView?.showError(str)
        }
}