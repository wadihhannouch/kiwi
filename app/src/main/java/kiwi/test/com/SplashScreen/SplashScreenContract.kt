package kiwi.test.com.SplashScreen

import kiwi.test.com.BaseContract

object SplashScreenContract {
    interface View : BaseContract.ActivityView {
        fun startMainActivity()
    }

    interface Presenter : BaseContract.ActivityPresenter<View> {
        fun onStartView()
    }
}