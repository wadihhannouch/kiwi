package kiwi.test.com.SplashScreen

import android.content.Intent
import android.os.Bundle
import kiwi.test.com.BaseActivity
import kiwi.test.com.Main.MainActivity
import com.bumptech.glide.Glide

import kotlinx.android.synthetic.main.activity_splash_screen.*
import com.bumptech.glide.GenericTransitionOptions
import kiwi.test.com.R


class SplashScreenActivity() : BaseActivity<SplashScreenContract.View, SplashScreenContract.Presenter>(),
    SplashScreenContract.View {

    override var mPresenter: SplashScreenContract.Presenter = SplashScreenActivityPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(kiwi.test.com.R.layout.activity_splash_screen);
        Glide.with(this).load(kiwi.test.com.R.drawable.splashlogo).transition(GenericTransitionOptions.with(R.anim.zoom_in)).into(imageView)
        mPresenter.onStartView()
    }
    companion object{
    }

    override fun startMainActivity() {
        val intent = Intent(context, MainActivity::class.java)
        startActivity(intent)
    }
}