package kiwi.test.com.SplashScreen

import android.os.Handler
import kiwi.test.com.BaseActivityPresenter


class SplashScreenActivityPresenter : BaseActivityPresenter<SplashScreenContract.View>(),
        SplashScreenContract.Presenter {

        override fun onStartView() {

                Handler().postDelayed( Runnable() {
                        run() {
                                mView?.startMainActivity()
                        }
                }, SPLASH_TIME_OUT.toLong());
        }
        companion object{
                val SPLASH_TIME_OUT = 1500;
        }
}