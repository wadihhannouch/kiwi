package kiwi.test.com

import io.reactivex.disposables.CompositeDisposable

open class BaseActivityPresenter<V : BaseContract.ActivityView> : BaseContract.ActivityPresenter<V> {

    protected var mView: V? = null

    init {

    }

    protected var mCompositeDisposable: CompositeDisposable? = null

    init {
        mCompositeDisposable = CompositeDisposable()
    }

    override fun attachView(view: V) {
        mView = view
    }

    override fun detachView() {
        mView = null
    }

    override fun onStop() {
        mCompositeDisposable?.clear()
    }
}