package kiwi.test.com

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.res.Configuration
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.MotionEvent
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

import java.util.*


abstract class BaseActivity<in V : BaseContract.ActivityView, T : BaseContract.ActivityPresenter<V>>
    : AppCompatActivity(), BaseContract.ActivityView {

    companion object {
        lateinit var context: Activity
        lateinit var alertDialog : AlertDialog
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        setAppLocale()
        mPresenter.attachView(this as V)
        context = this


    }

    override fun getContext(): Context = this

    protected abstract var mPresenter: T


    fun showAlert(msg:String?,title:String){
        AlertDialog.Builder(context).
            setTitle(title)
            .setMessage(msg)
            .setPositiveButton(android.R.string.yes,null)
            .setIcon(android.R.drawable.ic_dialog_alert)
            .show();
    }
    override fun showError(error: String?) {
        showAlert(error,"error!")
//        Toast.makeText(this, error, Toast.LENGTH_LONG).show()
    }

    override fun showError(stringResId: Int) {
        showAlert("$stringResId","error")
    }

    override fun showMessage(srtResId: Int) {
        showAlert("$srtResId","error")
    }

    override fun showMessage(message: String) {
        showAlert(message,"error")
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.detachView()
    }

    private fun setAppLocale() {
        var languageToLoad = ""
    }

    fun changeStatusBarColor(color: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.setStatusBarColor(getColor(color))
        }
    }
}