package kiwi.test.com

import android.content.Context
import androidx.annotation.StringRes

object BaseContract {
    interface ActivityPresenter<in V : ActivityView> {
        fun attachView(view: V)
        fun detachView()
        fun onStop()
    }

    interface ActivityView {
        fun getContext(): Context
        fun showError(error: String?)
        fun showError(@StringRes stringResId: Int)
        fun showMessage(@StringRes srtResId: Int)
        fun showMessage(message: String)
    }

    interface FragmentPresenter<in V : FragmentView> {
        fun attachView(view: V)
        fun onStop()
        fun detachView()
    }

    interface FragmentView {
        fun getContext(): Context
        fun showError(error: String?)
        fun showError(@StringRes stringResId: Int)
        fun showMessage(@StringRes srtResId: Int)
        fun showMessage(message: String)
        fun startAccountActivity(fragmentName: String?)
        fun finishAccountActivity()
    }
}