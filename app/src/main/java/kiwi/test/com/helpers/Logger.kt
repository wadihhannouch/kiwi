package kiwi.test.com.helpers

import android.util.Log

import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

object Logger {

    val LOG_ENABLED = true

    private var TAG = "Logger"
    private val sDateFormat = SimpleDateFormat(
            "MM-dd HH:mm:ss.ssss", Locale.getDefault()
    )

    fun setTag(tag: String) {
        TAG = tag
    }

    fun setLog(requestedBy: Class<*>, message: String, type: Type) {
        if (!LOG_ENABLED)
            return

        when (type) {
            Logger.Type.T_DEBUG -> setDebugLog(requestedBy, message)
            Logger.Type.T_WARNING -> setWarningLog(requestedBy, message)
            Logger.Type.T_ERROR -> setErrorLog(requestedBy, message)
            else -> setDefaultLog(requestedBy, message)
        }
    }

    private fun setDefaultLog(requestedBy: Class<*>, message: String) {
        Log.i(TAG + requestedBy.simpleName,
                "\n" +
                        "Type: [  DEFAULT  ]" + "\n" +
                        "Time: " + sDateFormat.format(Date(System.currentTimeMillis())) + "\n" +
                        "Class: " + requestedBy.simpleName + "\n" +
                        "Message: " + message
        )
    }

    private fun setDebugLog(requestedBy: Class<*>, message: String) {
        Log.d(TAG + requestedBy.simpleName,
                ("\n" +
                        "Type: [  DEBUG  ]" + "\n" +
                        "Time: " + sDateFormat.format(Date(System.currentTimeMillis())) + "\n" +
                        "Class: " + requestedBy.simpleName + "\n" +
                        "Message: " + message)
        )
    }

    private fun setWarningLog(requestedBy: Class<*>, message: String) {
        Log.w(TAG + requestedBy.simpleName,
                ("\n" +
                        "Type: [  WARNING  ]" + "\n" +
                        "Time: " + sDateFormat.format(Date(System.currentTimeMillis())) + "\n" +
                        "Class: " + requestedBy.simpleName + "\n" +
                        "Message: " + message)
        )
    }

    private fun setErrorLog(requestedBy: Class<*>, message: String) {
        Log.e(TAG + requestedBy.simpleName,
                ("\n" +
                        "Type: [  ERROR  ]" + "\n" +
                        "Time: " + sDateFormat.format(Date(System.currentTimeMillis())) + "\n" +
                        "Class: " + requestedBy.simpleName + "\n" +
                        "Message: " + message)
        )
    }

    enum class Type {
        T_DEFAULT, T_DEBUG, T_WARNING, T_ERROR
    }
}
