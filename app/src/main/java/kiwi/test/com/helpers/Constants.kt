package kiwi.test.com.helpers

object Constants {
    val API_URL = "https://api.skypicker.com"
    val Base_Image_URL = "https://images.kiwi.com/photos/600x330/"
    var Partner = "picky"
    val sort = "price"
    val dataVersion = "2"
    val limit = "45"
    enum class flightType (val type : String)
    {
        economy("M"),
        economy_premium("E"),
        business("C"),
        first_class("F")
    };
}
