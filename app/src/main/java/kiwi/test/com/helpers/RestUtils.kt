package kiwi.test.com.helpers

import android.accounts.NetworkErrorException

import com.google.gson.JsonParseException
import com.jakewharton.retrofit2.adapter.rxjava2.HttpException

import java.net.SocketTimeoutException
import java.util.concurrent.TimeoutException

object RestUtils {

    fun ErrorMessages(error: Throwable): String? {
        var message: String? = null
        Logger.setLog(RestUtils::class.java, error.message!!, Logger.Type.T_ERROR)
        if (error is NetworkErrorException) {
            message = "Cannot connect to Internet...Please check your connection."
        } else if (error is JsonParseException) {
            message = "Receiving data error. Possibly server under maintenance."
        } else if (error is TimeoutException) {
            message = "Connection TimeOut. Please check your internet connection."
        } else if (error is SocketTimeoutException) {
            message = "Connection TimeOut. Please check your internet connection."
        } else if (error is HttpException) {
            message = "Cannot connect to Internet...Please check your connection."
        } else if (message == null && error.localizedMessage != null) {
            //message = error.getMessage();
            message = error.localizedMessage
        } else {
            message = "Unknown exeption"
        }
        return message
    }
}
