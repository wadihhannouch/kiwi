
package kiwi.test.com.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Conversion {

    @SerializedName("EUR")
    @Expose
    private Integer eUR;

    public Integer getEUR() {
        return eUR;
    }

    public void setEUR(Integer eUR) {
        this.eUR = eUR;
    }

}
