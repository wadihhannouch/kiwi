
package kiwi.test.com.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BagsPrice {

    @SerializedName("1")
    @Expose
    private Double _1;

    public Double get1() {
        return _1;
    }

    public void set1(Double _1) {
        this._1 = _1;
    }

}
