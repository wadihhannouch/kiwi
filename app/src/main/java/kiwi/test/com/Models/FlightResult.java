
package kiwi.test.com.Models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FlightResult {

    @SerializedName("search_id")
    @Expose
    private String searchId;

    @SerializedName("data")
    @Expose
    private List<Flight> flight = null;

    @SerializedName("connections")
    @Expose
    private List<Object> connections = null;

    @SerializedName("time")
    @Expose
    private Integer time;

    @SerializedName("currency")
    @Expose
    private String currency;

    @SerializedName("currency_rate")
    @Expose
    private Integer currencyRate;

    @SerializedName("fx_rate")
    @Expose
    private Integer fxRate;

    @SerializedName("refresh")
    @Expose
    private List<Object> refresh = null;
    @SerializedName("del")
    @Expose
    private Double del;
    @SerializedName("ref_tasks")
    @Expose
    private List<Object> refTasks = null;
    @SerializedName("search_params")
    @Expose
    private SearchParams searchParams;
    @SerializedName("all_stopover_airports")
    @Expose
    private List<Object> allStopoverAirports = null;
    @SerializedName("all_airlines")
    @Expose
    private List<Object> allAirlines = null;

    public String getSearchId() {
        return searchId;
    }

    public void setSearchId(String searchId) {
        this.searchId = searchId;
    }

    public List<Object> getConnections() {
        return connections;
    }

    public void setConnections(List<Object> connections) {
        this.connections = connections;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Integer getCurrencyRate() {
        return currencyRate;
    }

    public void setCurrencyRate(Integer currencyRate) {
        this.currencyRate = currencyRate;
    }

    public Integer getFxRate() {
        return fxRate;
    }

    public void setFxRate(Integer fxRate) {
        this.fxRate = fxRate;
    }

    public List<Object> getRefresh() {
        return refresh;
    }

    public void setRefresh(List<Object> refresh) {
        this.refresh = refresh;
    }

    public Double getDel() {
        return del;
    }

    public void setDel(Double del) {
        this.del = del;
    }

    public List<Object> getRefTasks() {
        return refTasks;
    }

    public void setRefTasks(List<Object> refTasks) {
        this.refTasks = refTasks;
    }

    public SearchParams getSearchParams() {
        return searchParams;
    }

    public void setSearchParams(SearchParams searchParams) {
        this.searchParams = searchParams;
    }

    public List<Object> getAllStopoverAirports() {
        return allStopoverAirports;
    }

    public void setAllStopoverAirports(List<Object> allStopoverAirports) {
        this.allStopoverAirports = allStopoverAirports;
    }

    public List<Object> getAllAirlines() {
        return allAirlines;
    }

    public void setAllAirlines(List<Object> allAirlines) {
        this.allAirlines = allAirlines;
    }

    public List<Flight> getFlight() {
        return flight;
    }

    public void setFlight(List<Flight> flight) {
        this.flight = flight;
    }
}
