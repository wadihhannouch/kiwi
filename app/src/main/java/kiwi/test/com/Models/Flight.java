
package kiwi.test.com.Models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Flight {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("bags_price")
    @Expose
    private BagsPrice bagsPrice;
    @SerializedName("baglimit")
    @Expose
    private Baglimit baglimit;
    @SerializedName("p1")
    @Expose
    private Integer p1;
    @SerializedName("p2")
    @Expose
    private Integer p2;
    @SerializedName("p3")
    @Expose
    private Integer p3;
    @SerializedName("price")
    @Expose
    private Integer price;
    @SerializedName("route")
    @Expose
    private List<Route> route = null;
    @SerializedName("airlines")
    @Expose
    private List<String> airlines = null;
    @SerializedName("pnr_count")
    @Expose
    private Integer pnrCount;
    @SerializedName("transfers")
    @Expose
    private List<Object> transfers = null;
    @SerializedName("has_airport_change")
    @Expose
    private Boolean hasAirportChange;
    @SerializedName("availability")
    @Expose
    private Availability availability;
    @SerializedName("dTime")
    @Expose
    private Integer dTime;
    @SerializedName("dTimeUTC")
    @Expose
    private Integer dTimeUTC;
    @SerializedName("aTime")
    @Expose
    private Integer aTime;
    @SerializedName("aTimeUTC")
    @Expose
    private Integer aTimeUTC;
    @SerializedName("nightsInDest")
    @Expose
    private Object nightsInDest;
    @SerializedName("flyFrom")
    @Expose
    private String flyFrom;
    @SerializedName("flyTo")
    @Expose
    private String flyTo;
    @SerializedName("cityFrom")
    @Expose
    private String cityFrom;
    @SerializedName("cityTo")
    @Expose
    private String cityTo;
    @SerializedName("cityCodeFrom")
    @Expose
    private String cityCodeFrom;
    @SerializedName("cityCodeTo")
    @Expose
    private String cityCodeTo;
    @SerializedName("countryFrom")
    @Expose
    private CountryFrom countryFrom;
    @SerializedName("countryTo")
    @Expose
    private CountryTo countryTo;
    @SerializedName("mapIdfrom")
    @Expose
    private String mapIdfrom;
    @SerializedName("mapIdto")
    @Expose
    private String mapIdto;
    @SerializedName("distance")
    @Expose
    private Double distance;
    @SerializedName("routes")
    @Expose
    private List<List<String>> routes = null;
    @SerializedName("virtual_interlining")
    @Expose
    private Boolean virtualInterlining;
    @SerializedName("fly_duration")
    @Expose
    private String flyDuration;
    @SerializedName("duration")
    @Expose
    private Duration duration;
    @SerializedName("facilitated_booking_available")
    @Expose
    private Boolean facilitatedBookingAvailable;
    @SerializedName("type_flights")
    @Expose
    private List<String> typeFlights = null;
    @SerializedName("found_on")
    @Expose
    private List<String> foundOn = null;
    @SerializedName("conversion")
    @Expose
    private Conversion conversion;
    @SerializedName("booking_token")
    @Expose
    private String bookingToken;
    @SerializedName("quality")
    @Expose
    private Double quality;
    @SerializedName("deep_link")
    @Expose
    private String deepLink;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public BagsPrice getBagsPrice() {
        return bagsPrice;
    }

    public void setBagsPrice(BagsPrice bagsPrice) {
        this.bagsPrice = bagsPrice;
    }

    public Baglimit getBaglimit() {
        return baglimit;
    }

    public void setBaglimit(Baglimit baglimit) {
        this.baglimit = baglimit;
    }

    public Integer getP1() {
        return p1;
    }

    public void setP1(Integer p1) {
        this.p1 = p1;
    }

    public Integer getP2() {
        return p2;
    }

    public void setP2(Integer p2) {
        this.p2 = p2;
    }

    public Integer getP3() {
        return p3;
    }

    public void setP3(Integer p3) {
        this.p3 = p3;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public List<Route> getRoute() {
        return route;
    }

    public void setRoute(List<Route> route) {
        this.route = route;
    }

    public List<String> getAirlines() {
        return airlines;
    }

    public void setAirlines(List<String> airlines) {
        this.airlines = airlines;
    }

    public Integer getPnrCount() {
        return pnrCount;
    }

    public void setPnrCount(Integer pnrCount) {
        this.pnrCount = pnrCount;
    }

    public List<Object> getTransfers() {
        return transfers;
    }

    public void setTransfers(List<Object> transfers) {
        this.transfers = transfers;
    }

    public Boolean getHasAirportChange() {
        return hasAirportChange;
    }

    public void setHasAirportChange(Boolean hasAirportChange) {
        this.hasAirportChange = hasAirportChange;
    }

    public Availability getAvailability() {
        return availability;
    }

    public void setAvailability(Availability availability) {
        this.availability = availability;
    }

    public Integer getDTime() {
        return dTime;
    }

    public void setDTime(Integer dTime) {
        this.dTime = dTime;
    }

    public Integer getDTimeUTC() {
        return dTimeUTC;
    }

    public void setDTimeUTC(Integer dTimeUTC) {
        this.dTimeUTC = dTimeUTC;
    }

    public Integer getATime() {
        return aTime;
    }

    public void setATime(Integer aTime) {
        this.aTime = aTime;
    }

    public Integer getATimeUTC() {
        return aTimeUTC;
    }

    public void setATimeUTC(Integer aTimeUTC) {
        this.aTimeUTC = aTimeUTC;
    }

    public Object getNightsInDest() {
        return nightsInDest;
    }

    public void setNightsInDest(Object nightsInDest) {
        this.nightsInDest = nightsInDest;
    }

    public String getFlyFrom() {
        return flyFrom;
    }

    public void setFlyFrom(String flyFrom) {
        this.flyFrom = flyFrom;
    }

    public String getFlyTo() {
        return flyTo;
    }

    public void setFlyTo(String flyTo) {
        this.flyTo = flyTo;
    }

    public String getCityFrom() {
        return cityFrom;
    }

    public void setCityFrom(String cityFrom) {
        this.cityFrom = cityFrom;
    }

    public String getCityTo() {
        return cityTo;
    }

    public void setCityTo(String cityTo) {
        this.cityTo = cityTo;
    }

    public String getCityCodeFrom() {
        return cityCodeFrom;
    }

    public void setCityCodeFrom(String cityCodeFrom) {
        this.cityCodeFrom = cityCodeFrom;
    }

    public String getCityCodeTo() {
        return cityCodeTo;
    }

    public void setCityCodeTo(String cityCodeTo) {
        this.cityCodeTo = cityCodeTo;
    }

    public CountryFrom getCountryFrom() {
        return countryFrom;
    }

    public void setCountryFrom(CountryFrom countryFrom) {
        this.countryFrom = countryFrom;
    }

    public CountryTo getCountryTo() {
        return countryTo;
    }

    public void setCountryTo(CountryTo countryTo) {
        this.countryTo = countryTo;
    }

    public String getMapIdfrom() {
        return mapIdfrom;
    }

    public void setMapIdfrom(String mapIdfrom) {
        this.mapIdfrom = mapIdfrom;
    }

    public String getMapIdto() {
        return mapIdto;
    }

    public void setMapIdto(String mapIdto) {
        this.mapIdto = mapIdto;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public List<List<String>> getRoutes() {
        return routes;
    }

    public void setRoutes(List<List<String>> routes) {
        this.routes = routes;
    }

    public Boolean getVirtualInterlining() {
        return virtualInterlining;
    }

    public void setVirtualInterlining(Boolean virtualInterlining) {
        this.virtualInterlining = virtualInterlining;
    }

    public String getFlyDuration() {
        return flyDuration;
    }

    public void setFlyDuration(String flyDuration) {
        this.flyDuration = flyDuration;
    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public Boolean getFacilitatedBookingAvailable() {
        return facilitatedBookingAvailable;
    }

    public void setFacilitatedBookingAvailable(Boolean facilitatedBookingAvailable) {
        this.facilitatedBookingAvailable = facilitatedBookingAvailable;
    }

    public List<String> getTypeFlights() {
        return typeFlights;
    }

    public void setTypeFlights(List<String> typeFlights) {
        this.typeFlights = typeFlights;
    }

    public List<String> getFoundOn() {
        return foundOn;
    }

    public void setFoundOn(List<String> foundOn) {
        this.foundOn = foundOn;
    }

    public Conversion getConversion() {
        return conversion;
    }

    public void setConversion(Conversion conversion) {
        this.conversion = conversion;
    }

    public String getBookingToken() {
        return bookingToken;
    }

    public void setBookingToken(String bookingToken) {
        this.bookingToken = bookingToken;
    }

    public Double getQuality() {
        return quality;
    }

    public void setQuality(Double quality) {
        this.quality = quality;
    }

    public String getDeepLink() {
        return deepLink;
    }

    public void setDeepLink(String deepLink) {
        this.deepLink = deepLink;
    }

}
