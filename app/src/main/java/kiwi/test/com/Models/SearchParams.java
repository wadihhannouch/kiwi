
package kiwi.test.com.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchParams {

    @SerializedName("flyFrom_type")
    @Expose
    private String flyFromType;
    @SerializedName("to_type")
    @Expose
    private String toType;
    @SerializedName("seats")
    @Expose
    private Seats seats;

    public String getFlyFromType() {
        return flyFromType;
    }

    public void setFlyFromType(String flyFromType) {
        this.flyFromType = flyFromType;
    }

    public String getToType() {
        return toType;
    }

    public void setToType(String toType) {
        this.toType = toType;
    }

    public Seats getSeats() {
        return seats;
    }

    public void setSeats(Seats seats) {
        this.seats = seats;
    }

}
