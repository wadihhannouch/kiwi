
package kiwi.test.com.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Route {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("combination_id")
    @Expose
    private String combinationId;
    @SerializedName("return")
    @Expose
    private Integer _return;
    @SerializedName("original_return")
    @Expose
    private Integer originalReturn;
    @SerializedName("source")
    @Expose
    private String source;
    @SerializedName("found_on")
    @Expose
    private String foundOn;
    @SerializedName("price")
    @Expose
    private Integer price;
    @SerializedName("aTime")
    @Expose
    private Integer aTime;
    @SerializedName("dTime")
    @Expose
    private Integer dTime;
    @SerializedName("aTimeUTC")
    @Expose
    private Integer aTimeUTC;
    @SerializedName("dTimeUTC")
    @Expose
    private Integer dTimeUTC;
    @SerializedName("mapIdfrom")
    @Expose
    private String mapIdfrom;
    @SerializedName("mapIdto")
    @Expose
    private String mapIdto;
    @SerializedName("cityTo")
    @Expose
    private String cityTo;
    @SerializedName("cityFrom")
    @Expose
    private String cityFrom;
    @SerializedName("cityCodeFrom")
    @Expose
    private String cityCodeFrom;
    @SerializedName("cityCodeTo")
    @Expose
    private String cityCodeTo;
    @SerializedName("flyTo")
    @Expose
    private String flyTo;
    @SerializedName("airline")
    @Expose
    private String airline;
    @SerializedName("operating_carrier")
    @Expose
    private String operatingCarrier;
    @SerializedName("equipment")
    @Expose
    private String equipment;
    @SerializedName("flyFrom")
    @Expose
    private String flyFrom;
    @SerializedName("latFrom")
    @Expose
    private Double latFrom;
    @SerializedName("lngFrom")
    @Expose
    private Double lngFrom;
    @SerializedName("latTo")
    @Expose
    private Double latTo;
    @SerializedName("lngTo")
    @Expose
    private Double lngTo;
    @SerializedName("flight_no")
    @Expose
    private Integer flightNo;
    @SerializedName("vehicle_type")
    @Expose
    private String vehicleType;
    @SerializedName("refresh_timestamp")
    @Expose
    private Integer refreshTimestamp;
    @SerializedName("bags_recheck_required")
    @Expose
    private Boolean bagsRecheckRequired;
    @SerializedName("guarantee")
    @Expose
    private Boolean guarantee;
    @SerializedName("fare_classes")
    @Expose
    private String fareClasses;
    @SerializedName("fare_basis")
    @Expose
    private String fareBasis;
    @SerializedName("fare_family")
    @Expose
    private String fareFamily;
    @SerializedName("fare_category")
    @Expose
    private String fareCategory;
    @SerializedName("last_seen")
    @Expose
    private Integer lastSeen;
    @SerializedName("operating_flight_no")
    @Expose
    private String operatingFlightNo;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCombinationId() {
        return combinationId;
    }

    public void setCombinationId(String combinationId) {
        this.combinationId = combinationId;
    }

    public Integer getReturn() {
        return _return;
    }

    public void setReturn(Integer _return) {
        this._return = _return;
    }

    public Integer getOriginalReturn() {
        return originalReturn;
    }

    public void setOriginalReturn(Integer originalReturn) {
        this.originalReturn = originalReturn;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getFoundOn() {
        return foundOn;
    }

    public void setFoundOn(String foundOn) {
        this.foundOn = foundOn;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getATime() {
        return aTime;
    }

    public void setATime(Integer aTime) {
        this.aTime = aTime;
    }

    public Integer getDTime() {
        return dTime;
    }

    public void setDTime(Integer dTime) {
        this.dTime = dTime;
    }

    public Integer getATimeUTC() {
        return aTimeUTC;
    }

    public void setATimeUTC(Integer aTimeUTC) {
        this.aTimeUTC = aTimeUTC;
    }

    public Integer getDTimeUTC() {
        return dTimeUTC;
    }

    public void setDTimeUTC(Integer dTimeUTC) {
        this.dTimeUTC = dTimeUTC;
    }

    public String getMapIdfrom() {
        return mapIdfrom;
    }

    public void setMapIdfrom(String mapIdfrom) {
        this.mapIdfrom = mapIdfrom;
    }

    public String getMapIdto() {
        return mapIdto;
    }

    public void setMapIdto(String mapIdto) {
        this.mapIdto = mapIdto;
    }

    public String getCityTo() {
        return cityTo;
    }

    public void setCityTo(String cityTo) {
        this.cityTo = cityTo;
    }

    public String getCityFrom() {
        return cityFrom;
    }

    public void setCityFrom(String cityFrom) {
        this.cityFrom = cityFrom;
    }

    public String getCityCodeFrom() {
        return cityCodeFrom;
    }

    public void setCityCodeFrom(String cityCodeFrom) {
        this.cityCodeFrom = cityCodeFrom;
    }

    public String getCityCodeTo() {
        return cityCodeTo;
    }

    public void setCityCodeTo(String cityCodeTo) {
        this.cityCodeTo = cityCodeTo;
    }

    public String getFlyTo() {
        return flyTo;
    }

    public void setFlyTo(String flyTo) {
        this.flyTo = flyTo;
    }

    public String getAirline() {
        return airline;
    }

    public void setAirline(String airline) {
        this.airline = airline;
    }

    public String getOperatingCarrier() {
        return operatingCarrier;
    }

    public void setOperatingCarrier(String operatingCarrier) {
        this.operatingCarrier = operatingCarrier;
    }

    public String getEquipment() {
        return equipment;
    }

    public void setEquipment(String equipment) {
        this.equipment = equipment;
    }

    public String getFlyFrom() {
        return flyFrom;
    }

    public void setFlyFrom(String flyFrom) {
        this.flyFrom = flyFrom;
    }

    public Double getLatFrom() {
        return latFrom;
    }

    public void setLatFrom(Double latFrom) {
        this.latFrom = latFrom;
    }

    public Double getLngFrom() {
        return lngFrom;
    }

    public void setLngFrom(Double lngFrom) {
        this.lngFrom = lngFrom;
    }

    public Double getLatTo() {
        return latTo;
    }

    public void setLatTo(Double latTo) {
        this.latTo = latTo;
    }

    public Double getLngTo() {
        return lngTo;
    }

    public void setLngTo(Double lngTo) {
        this.lngTo = lngTo;
    }

    public Integer getFlightNo() {
        return flightNo;
    }

    public void setFlightNo(Integer flightNo) {
        this.flightNo = flightNo;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public Integer getRefreshTimestamp() {
        return refreshTimestamp;
    }

    public void setRefreshTimestamp(Integer refreshTimestamp) {
        this.refreshTimestamp = refreshTimestamp;
    }

    public Boolean getBagsRecheckRequired() {
        return bagsRecheckRequired;
    }

    public void setBagsRecheckRequired(Boolean bagsRecheckRequired) {
        this.bagsRecheckRequired = bagsRecheckRequired;
    }

    public Boolean getGuarantee() {
        return guarantee;
    }

    public void setGuarantee(Boolean guarantee) {
        this.guarantee = guarantee;
    }

    public String getFareClasses() {
        return fareClasses;
    }

    public void setFareClasses(String fareClasses) {
        this.fareClasses = fareClasses;
    }

    public String getFareBasis() {
        return fareBasis;
    }

    public void setFareBasis(String fareBasis) {
        this.fareBasis = fareBasis;
    }

    public String getFareFamily() {
        return fareFamily;
    }

    public void setFareFamily(String fareFamily) {
        this.fareFamily = fareFamily;
    }

    public String getFareCategory() {
        return fareCategory;
    }

    public void setFareCategory(String fareCategory) {
        this.fareCategory = fareCategory;
    }

    public Integer getLastSeen() {
        return lastSeen;
    }

    public void setLastSeen(Integer lastSeen) {
        this.lastSeen = lastSeen;
    }

    public String getOperatingFlightNo() {
        return operatingFlightNo;
    }

    public void setOperatingFlightNo(String operatingFlightNo) {
        this.operatingFlightNo = operatingFlightNo;
    }

}
