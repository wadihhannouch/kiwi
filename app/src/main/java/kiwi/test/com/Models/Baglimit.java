
package kiwi.test.com.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Baglimit {

    @SerializedName("hold_width")
    @Expose
    private Integer holdWidth;
    @SerializedName("hold_height")
    @Expose
    private Integer holdHeight;
    @SerializedName("hold_length")
    @Expose
    private Integer holdLength;
    @SerializedName("hold_dimensions_sum")
    @Expose
    private Integer holdDimensionsSum;
    @SerializedName("hold_weight")
    @Expose
    private Integer holdWeight;
    @SerializedName("hand_width")
    @Expose
    private Integer handWidth;
    @SerializedName("hand_height")
    @Expose
    private Integer handHeight;
    @SerializedName("hand_length")
    @Expose
    private Integer handLength;
    @SerializedName("hand_weight")
    @Expose
    private Integer handWeight;

    public Integer getHoldWidth() {
        return holdWidth;
    }

    public void setHoldWidth(Integer holdWidth) {
        this.holdWidth = holdWidth;
    }

    public Integer getHoldHeight() {
        return holdHeight;
    }

    public void setHoldHeight(Integer holdHeight) {
        this.holdHeight = holdHeight;
    }

    public Integer getHoldLength() {
        return holdLength;
    }

    public void setHoldLength(Integer holdLength) {
        this.holdLength = holdLength;
    }

    public Integer getHoldDimensionsSum() {
        return holdDimensionsSum;
    }

    public void setHoldDimensionsSum(Integer holdDimensionsSum) {
        this.holdDimensionsSum = holdDimensionsSum;
    }

    public Integer getHoldWeight() {
        return holdWeight;
    }

    public void setHoldWeight(Integer holdWeight) {
        this.holdWeight = holdWeight;
    }

    public Integer getHandWidth() {
        return handWidth;
    }

    public void setHandWidth(Integer handWidth) {
        this.handWidth = handWidth;
    }

    public Integer getHandHeight() {
        return handHeight;
    }

    public void setHandHeight(Integer handHeight) {
        this.handHeight = handHeight;
    }

    public Integer getHandLength() {
        return handLength;
    }

    public void setHandLength(Integer handLength) {
        this.handLength = handLength;
    }

    public Integer getHandWeight() {
        return handWeight;
    }

    public void setHandWeight(Integer handWeight) {
        this.handWeight = handWeight;
    }

}
