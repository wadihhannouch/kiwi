
package kiwi.test.com.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Availability {

    @SerializedName("seats")
    @Expose
    private Object seats;

    public Object getSeats() {
        return seats;
    }

    public void setSeats(Object seats) {
        this.seats = seats;
    }

}
